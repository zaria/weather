#!/usr/bin/env python3

"""
Weather module
Retrieves weather informations
"""

import os
import urllib.request
import urllib.error


try:
    token_file = open(os.path.dirname(__file__) + "/TOKEN", "r")
    TOKEN = token_file.readline().replace("\n", "")
    token_file.close()
except FileNotFoundError:
    print("Weather module: cannot find your API token, read README first")
    exit(1)


URL = "https://api.openweathermap.org/data/2.5/weather?APPID={}&".format(TOKEN)


def get_weather_by_name(name):
    """
    get weather from given location using city name
    expected format: <city name>,<country code>
      country code following ISO 3166 format

    example: Paris,fr
    """
    data = urllib.request.urlopen("{}q={}".format(URL, name))
    return data.read()


def get_weather_by_id(city_id):
    """
    get weather from given location using city ID
    see here (http://bulk.openweathermap.org/sample/)
    """
    data = urllib.request.urlopen("{}id={}".format(URL, city_id))
    return data.read()
