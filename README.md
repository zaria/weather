# Weather

`weather` plugin is a set of tools to get meteo informations.

## Installation

You will need an API key from [OpenWeatherMap](https://openweathermap.org/).
Then, simply create a file named `TOKEN` containing this line:

```
<your API key>
```

## Specification

Here are all available commands:

```
get_weather_by_id(id)
    get weather from given location using city ID
    see here (http://bulk.openweathermap.org/sample/)
    
get_weather_by_name(name)
    get weather from given location using city name
    expected format: <city name>,<country code>
      country code following ISO 3166 format

    example: Paris,fr
```
